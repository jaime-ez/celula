'use strict'

const identity = require('./lib/identity')
const replicate = require('./lib/gce/index')
const getClaims = require('./lib/getClaims')
const getKeys = require('./lib/getKeys')
const verifyClaim = require('./lib/verifyClaim')
const replicationRequest = require('./lib/replicationRequest')

const uuid = require('uuid')
const https = require('https')
const selfsigned = require('selfsigned')
const Koa = require('koa')
const _ = require('koa-route')
const bodyParser = require('koa-bodyparser')
const app = new Koa()
app.use(bodyParser())

const generation = process.env.CELULA_GEN ? process.env.CELULA_GEN : '0'

const celula = {
  getId: (ctx) => {
    return getKeys()
    .then((value) => {
      ctx.body = {
        publicKey: value.publicKey.toString('base64'),
        generation: value.generation
      }
    })
    .catch((err) => {
      ctx.throw(err)
    })
  },
  getClaims: (ctx, uuid) => {
    return getClaims(uuid)
    .then((value) => {
      ctx.body = value
    })
    .catch((err) => {
      if (err && err.notFound) {
        return ctx.throw(404)
      }
      if (err && err.statusCode) {
        return ctx.throw(err.statusCode, err.message)
      }
      return ctx.throw(500, err)
    })
  },
  verify: (ctx) => {
    return verifyClaim(ctx)
    .then((value) => {
      ctx.body = value
    })
    .catch((err) => {
      if (err.message === '400') {
        ctx.throw(400)
      } else {
        ctx.throw(err)
      }
    })
  },
  replicate: (ctx) => {
    ctx.request.body.uuid = uuid.v4()
    // return inmediately with replication request id and then process
    return replicationRequest.save(ctx.request.body, 'processing')
    .then((data) => {
      ctx.body = data
      replicate(ctx.request.body)
      .then((value) => {
        replicationRequest.save(ctx.request.body, 'success')
      })
      .catch((err) => {
        replicationRequest.save(ctx.request.body, 'error:' + err.message)
      })
    })
    .catch((err) => {
      return ctx.throw(500, err)
    })
  },
  getReplicationRequest: (ctx, uuid) => {
    return replicationRequest.get(uuid)
    .then((value) => {
      ctx.body = value
    })
    .catch((err) => {
      if (err && err.notFound) {
        return ctx.throw(404)
      }
      if (err && err.statusCode) {
        return ctx.throw(err.statusCode, err.message)
      }
      return ctx.throw(500, err)
    })
  }
}

app.use(_.get('/id', celula.getId))
app.use(_.get('/claims/:uuid', celula.getClaims))
app.use(_.get('/replicate/:uuid', celula.getReplicationRequest))
app.use(_.post('/verify', celula.verify))
app.use(_.post('/replicate', celula.replicate))

identity(generation)
.then(() => {
  // create ssl certs and server
  let pems = selfsigned.generate([{ name: 'commonName', value: 'celula' }], { days: 10000 })
  https.createServer({
    key: pems.private,
    cert: pems.cert
  }, app.callback()).listen(3141)
  console.log('listening over https on port 3141')
})
.catch((err) => {
  console.error('identityError:', err)
})
