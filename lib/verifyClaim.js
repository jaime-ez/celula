'use strict'

const signatures = require('sodium-signatures')
const getKeys = require('./getKeys')

let verify = (ctx) => {
  return new Promise((resolve, reject) => {
    if (!(ctx.request.body.claim && typeof ctx.request.body.claim === 'object' &&
    ctx.request.body.claim.message && typeof ctx.request.body.claim.message === 'object' &&
    ctx.request.body.claim.signature && typeof ctx.request.body.claim.signature === 'string')) {
      return reject(new Error('400'))
    }
    getKeys()
    .then((value) => {
      let verified = signatures.verify(Buffer.from(JSON.stringify(ctx.request.body.claim.message)), Buffer.from(ctx.request.body.claim.signature, 'base64'), value.publicKey)

      resolve(verified)
    })
    .catch((err) => {
      reject(err)
    })
  })
}

module.exports = verify
