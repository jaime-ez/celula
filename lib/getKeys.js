'use strict'

const level = require('level')

let getKeys = () => {
  return new Promise((resolve, reject) => {
    let db = level('./db', {valueEncoding: 'json'})
    db.get('keyPair', (err, value) => {
      if (err) {
        db.close(() => {
          reject(err)
        })
      } else {
        db.close(() => {
          resolve({
            publicKey: Buffer.from(value.publicKey.data),
            secretKey: Buffer.from(value.secretKey.data),
            generation: value.generation
          })
        })
      }
    })
  })
}

module.exports = getKeys
